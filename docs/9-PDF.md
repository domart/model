# Génération d'un PDF

Il suffit d'ajouter `mkdocs-page-pdf` dans le fichier `requirements.txt`, et d'ajouter `page-to-pdf` dans la liste des plugins dans le fichier `mkdocs.yml`.

La documentation est ici : [https://github.com/brospars/mkdocs-page-pdf](https://github.com/brospars/mkdocs-page-pdf).

