# Code

## Intro

Bla bla

=== "Format _JSON_"
            
    <center>
        ![postman JSON](./images/postman4.png)
    </center>

    ```json
    {
        "nightPrice": 654.3,
        "address": "64 avenue Jean Portalis",
        "city": "Tours",
        "zipCode": 37200
    }
    ```

=== "Format _XML_"

    <center>
        ![postman XML](./images/postman3.png)
    </center>
    
    ```xml
    <locationBean>
        <nightPrice>654.3</nightPrice>
        <address>64 avenue Jean Portalis</address>
        <city>Tours</city>
        <zipCode>37200</zipCode>
    </locationBean>
    ```

bla bla

## Première partie

Créer un WS Rest exposant les méthodes suivantes :

1. Récupération d'une location (à partir de son identifiant).
2. Récupération des toutes les locations disponibles.

    - Il doit être possible de trier les locations (sur l'id uniquement) de manière croissante ou décroissante.
    - Il doit être possible de filtrer les locations sur une ville.

3. Suppression d'une location (à partir de son identifiant).
4. Suppression de toutes les locations.
5. Création d'une location.
6. Mise à jour totale d'une location (à partir de son identifiant).
7. Mise à jour partielle d'une location (à partir de son identifiant).


???+exercice "Exercice 1 - Spécification de l'API"

    Réfléchissez à la structure de vos URLs, et au verbe HTTP utilisé pour implémenter cette liste de service.
    
    :warning: Il faut bien sûr respecter les différentes règles d'une architecture REST (les 3 premières en fait) (_Cf. cours à partir de la page 30_).


???+exercice "Exercice 2 - Affichage de location(s)"

    === "Énoncé"

        Implémenter les 2 premiers services.
        
        Dans les deux cas, le client doit pouvoir choisir de recevoir les données en XML ou en JSON. Si la réponse est demandée dans un autre format, une erreur `406` doit être retournée.
        
        Pour tester les Web Services Rest, nous allons utiliser _Postman_ (Cf. [ici](index.md#2-autres-outils-utiles)).
        
        <center>
			![postman1](./images/postman1.png)
		</center>
        
        1. On sélectionne le verbe HTTP souhaité.
        2. On indique l'URL permettant d'accéder au service souhaité.
        3. On sélectionne l'onglet _Params_,
        4. et on indique les éventuels paramètres de requête (les `QueryParam`).
        
            <center>
                ![postman2](./images/postman2.png)
            </center>
            
        5. On sélectionne l'onglet _Headers_, si on souhaite ajouter des informations dans l'en-tête de la requête HTTP,
        6. par exemple pour indiquer ce qu'on souhaite comme type de retour.
        7. On envoie la requête.
        8. On obtient le code HTTP retour,
        9. et en dessous, l'éventuel résultat de la requête.
    
    === "Aide"
    
        Nous allons utilises pour JaxRS pour implémenter des Web Services Rest en JEE (JaxRS est une spécification, Jackson est l'implémentation de cette spécification). _Cf cours à partir de la page 46._
        
        Pour cela, il suffit de créer une classe, `WebApp` par exemple, qui implémente `javax.ws.rs.core.Application`, et qui contient l'annotation `@ApplicationPath`.
        
        Il faut ensuite créer une seconde classe, qui sera le Web Service. Elle doit contenir une annotation `@Path`. Chaque méthode qui correspond à un service doit également contenir cette annotation, ainsi que celle correspondant au verbe HTTP qui permet de l'appeler.
        
        Concernant les paramètres des méthodes, il faut utiliser les annotations `@QueryParam`, `@PathParam`, `@HeaderParam` ou `@FormParam` en fonction du cas (Cf. cours).
        
        Il faut utiliser l'annotation `@Produces`, et utiliser les `MediaType` (en faisant attention au package) pour indiquer que XML et JSON peuvent être générés. On peut ici indiquer une liste de formats acceptés, entre `{}`. Si un autre format est demandée, une erreur `406` est automatiquement renvoyée.<br>
        Attention, pour générer du XML, il faut indiquer qu'il faut utiliser JaxB, ce qui n'est pas automatique ici (contrairement à lorsqu'on utilise JaxWS). Pour cela, il faut rajouter l'annotation `@XMLRootElement` sur le bean correspondant.<br>
        Pour que les annotations de JaxB (par exemple `@XmlTransient`) fonctionnenent également lors du mapping en JSON, il faut ajouter le JAR `jackson-annotations.jar` dans le dossier 📂`/WEB-INF/lib` du projet (il n'y a pas besoin de l'ajouter dans le _classpath_, il faut uniquement qu'il soit présent dans ce dossier, comme les _drivers_ JDBC). Vous pouvez télécharger la version 2.13.2 de ce JAR, [ici](https://celene.univ-tours.fr/pluginfile.php/1807709/mod_resource/content/1/jackson-annotations-2.13.2.jar).<br>
        Côté client, il faut ajouter la clef `Accept` dans l'en-tête de la requête, avec la valeur `application/json` ou `application/xml`.
        
        Enfin, pour pouvoir appeler les EJBs depuis cette classe, il faut rajouter l'annotation `@Stateless` sur la classe.



## 2ème partie

???+exercice "Exercice 3 - Suppression de location(s)"

    === "Énoncé"

        Implémenter les services de suppression (3 et 4).
        
        Pour ceux-ci, l'utilisateur doit être identifié. Nous n'allons pas géré ici toute la partie authentification, mais nous allons imaginer que l'authentification est gérée en amont, et que lorsque l'utilisateur est correctement identifié, un jeton d'authentification lui est renvoyé. Ce jeton est ensuite utilisé, et ajouté dans le _header_ des requêtes HTTP qui le nécessitent. Ce jeton doit être associé au code `Authorization` (la liste des en-tête est disponible [ici](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers){:target="_blank"}).

        <center>
            ``` mermaid
            sequenceDiagram
                participant Client du WS
                participant Serveur (le WS)
                Client du WS ->> Serveur (le WS): Connexion
                Serveur (le WS) -->> Client du WS: Token envoyé (ici 42)
                Client du WS ->> Serveur (le WS): Requêtes Rest ...
            ```
        </center>

        Ici, on se contentera de vérifier que le jeton `Authorization` présent dans l'en-tête de la requête HTTP est `42`. Si la clef `Authorization` n'est pas présente dans l'en-tête, une erreur `401` doit être renvoyée. Si cette clef est bien présente, mais avec une valeur autre que `42`, une erreur `403` doit être renvoyée. La liste des codes HTTP est disponible [ici](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP){:target="_blank"}.
                
        Pour le service de suppression d'une location donnée, si cette location n'existe pas, il faut renvoyer une erreur `404`.
    
    === "Aide"
    
        1. **Gérer l'en-tête de la requête HTTP.**
        
            Il faut penser à utiliser l'annotation `@HeaderParam` sur les paramètres concernés. Il faut utiliser l'interface `HttpHeaders` pour les noms de ces paramètres, par exemple :
            
            ```java
            @HeaderParam(HttpHeaders.AUTHORIZATION)
            ```
        2. **Indiquer un code retour HTTP.**
        
            Pour cela, il faut que la méthode qui implémente le service retourne un objet `Response`. Pour renvoyer un `401` par exemple, il suffira d'écrire le code suivant :
            
            ```java
            return Response.status(Status.UNAUTHORIZED).build();
            ```
            
            Pour indiquer un code `200`, on peut directement écrire :
            
            ```java
            return Response.ok().build();
            ```


???+exercice "Exercice 4 - Création de location"

    === "Énoncé"

        Implémenter le service de création (5).
        
        Pour celui-ci également, l'utilisateur doit être identifié.
        
        Il faut pouvoir l'appeler via un formulaire HTML, ou en envoyant directement toutes les informations dans le corps de la requête, au format XML ou JSON. Si un autre format est utilisé, une erreur `415` doit être renvoyée.
    
    === "Aide"
    
        1. **Créer une location**
        
            Il y a deux manières de créer une location :
            
            1. En envoyant directement toutes les informations dans le corps de la requête, en JSON ou en XML par exemple.
            2. En envoyant des informations en paramètres de la requête, exactement comme lorsqu'on soumet un formulaire HTML. On utilise alors des `FormParam`.
            
            Il va donc y avoir deux méthodes pour créer une location, donc les signatures sont :
            
            ```java
            // Méthode appelée lorsqu'on ajoute toutes les informations dans le corps de la requête.
            public void createLocation(LocationBean bean) {
                ...
            }
            
            // Méthode appelée lorsqu'on soumet un formulaire HTML.
            public void createLocation(@FormParam("price") Double nightPrice,
                    @FormParam("address") String address,
                    @FormParam("city") String city,
                    @FormParam("zipCode") String zipCode) {
                ...
            }
            ```
            
            **Pour appeler la première méthode, il faut :**
            
            1. Sélectionner l'onglet _Body_,
            2. cocher _raw_,
            3. sélectionner _JSON_ ou _XML_,
            4. et enfin indiquer les données, au format _JSON_ ou _XML_ selon :
            
            === "Format _JSON_"
            
                <center>
                    ![postman JSON](./images/postman4.png)
                </center>
            
                ```json
                {
                    "nightPrice": 654.3,
                    "address": "64 avenue Jean Portalis",
                    "city": "Tours",
                    "zipCode": 37200
                }
                ```
            
            === "Format _XML_"
            
                <center>
                    ![postman XML](./images/postman3.png)
                </center>
                
                ```xml
                <locationBean>
                    <nightPrice>654.3</nightPrice>
                    <address>64 avenue Jean Portalis</address>
                    <city>Tours</city>
                    <zipCode>37200</zipCode>
                </locationBean>
                ```
            
            ___
            
            **Pour appeler la deuxième méthode, il faut :**
            
            1. Sélectionner l'onglet _Body_,
            2. cocher _x-www-form-urlencoded_,
            3. et enfin indiquer les données, comme paramètre de la requête (`@FormParam`) :
            
            <center>
                ![postman5](./images/postman5.png)
            </center>
        
        3. **Obtenir une erreur `415` si le format n'est pas le bon.**
        
            Cela est automatique, à partir du moment où tout les format acceptés sont recensés explicitement dans les annotations `@Consumes` des deux méthodes de création.


???+exercice "Exercice 5 - Mise à jour de location"

    1. Ajouter une méthode de mise à jour d'une location dans le DAO.
    2. Ajouter une méthode de mise à jour d'une location dans l'EJB.
    3. Implémenter les services permettant une mise à jour totale (6) et une mise à jour partielle (7) d'une location.
        
        On implémentera que la partie concernant la mise à jour via des données envoyées en JSON ou en XML. On n'implémentera pas la partie concernant la mise à jour d'une location via un formulaire HTML.
        
        Pour ces services également, l'utilisateur doit être identifié.